package test;

import org.junit.Before;
import org.junit.Test;
import problems.Raiz;
import problems.RaizImpl;

import static org.junit.Assert.assertEquals;

public class RaizTest {

    private Raiz implementation;

    @Before
    public void setUp() {
        implementation = new RaizImpl();
    }

    @Test
    public void testaRaizExata() {
        assertEquals(2, implementation.raiz(8, 3, 0), 0);
    }

    @Test
    public void testaRaizAproximada() {
        assertEquals(1.7, implementation.raiz(3, 2, 0.3), 1);
    }
}

package problems;

/**
 * Classe com metodos para calcular raiz n-esima de um numero com aproximacao
 * e para encontrar os limites que dividem um array em 3 partes de mesmo tamanho
 * 
 * @author adalbertocajueiro
 *
 */
public class RaizImpl implements Raiz {

	public double raiz(int numero, int raiz, double erro) {
  		return buscaBinaria(numero, raiz, erro, 0, numero);
  	}

  	private double buscaBinaria(int numero, int raiz, double erro, double left, double right) {
  		double middle = (left + right) / 2;
  		double potencia = calculaPotencia(middle, raiz);

  		if (potencia == numero) {
  			return middle;
  		} else if (potencia < numero) {
  			double diferenca = numero - potencia;
  			if (diferenca <= erro) {
  				return middle;
  			} else {
  				return buscaBinaria(numero, raiz, erro, middle + 1, right);
  			}
  		} else {
  			double diferenca = potencia - numero;
  			if (diferenca <= erro) {
  				return middle;
  			} else {
  				return buscaBinaria(numero, raiz, erro, left, middle - 1);
  			}
  		}
  	}

  	private double calculaPotencia(double x, int n) {
  		if (n == 0) {
  			return 1;
  		} else {
  			return x * calculaPotencia(x, n-1);
		}
  	}

}